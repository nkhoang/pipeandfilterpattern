# Demo

## Introduction

This is a small program to demonstrate an implementation of _Pipes and Filters Pattern_. The prograam is designed to 
filter text from a source file based on a provided pattern (simple string) 
1. The pattern (default to `Pattern`)
2. The path to the test file (default to `test.txt` in the `resources` directory)

Run the demo using the command below

```
mvn clean compile exec:exec
```

## Expected Output
```
com.kms.PipeAndFilter [main] >>>> Pipeline is going to start filtering file /Users/hoangknguyen/Projects/em/PipeAndFilterDemo/src/main/resources/test.txt using pattern [Pipe and Filter]
Pipe and Filter testing, Pipe and Filter testing, Pipe and Filter testing
Pipe and Filter will show
```
