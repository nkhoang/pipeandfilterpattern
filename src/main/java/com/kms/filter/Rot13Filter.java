package com.kms.filter;

import com.kms.Pipe;
import com.kms.PipeFilter;
import com.kms.utils.Rot13;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.io.Reader;
import java.io.Writer;

/**
 * @author hnguyen created on 10/22/18.
 */
@Slf4j
public class Rot13Filter extends PipeFilter {
    public Rot13Filter(Pipe pipe) throws IOException {
        super(pipe);
    }

    @Override
    public void filter(Reader in, Writer out) throws IOException {
        if (log.isDebugEnabled()) {
            log.debug("{} is filtering...", this.getName());
        }
        char[] buffer = new char[1024];
        int charsRead;

        while ((charsRead = in.read(buffer)) != -1) {
            for (int i = 0; i < charsRead; i++) {
                buffer[i] = Rot13.convert(buffer[i]);
            }
            out.write(buffer, 0, charsRead);
        }
    }
}
