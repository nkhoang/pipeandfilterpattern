package com.kms.filter;

import com.kms.Pipe;
import com.kms.PipeFilter;
import com.kms.utils.GrepReader;
import lombok.extern.slf4j.Slf4j;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.Reader;
import java.io.Writer;

/**
 * @author hnguyen created on 10/22/18.
 */
@Slf4j
public class GrepFilter extends PipeFilter {
    private String pattern;

    public GrepFilter(Pipe sink, String pattern) throws IOException {
        super(sink);
        this.pattern = pattern;
    }

    @Override
    public void filter(Reader in, Writer out) throws IOException {
        if (log.isDebugEnabled()) {
            log.debug("{} is filtering using pattern [{}]", this.getName(), pattern);
        }
        GrepReader gr = new GrepReader(new BufferedReader(in), pattern);
        PrintWriter pw = new PrintWriter(out);
        String line;

        while ((line = gr.readLine()) != null) {
            pw.println(line);
        }
    }


}
