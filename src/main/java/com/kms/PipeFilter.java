package com.kms;

import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.io.Reader;
import java.io.Writer;

/**
 * @author hnguyen created on 10/22/18.
 */
@Slf4j
public abstract class PipeFilter extends Pipe {
    public PipeFilter(Pipe pipe) throws IOException {
        super(pipe);
    }

    @Override
    public void run() {
        try {
            filter(in, out);
        } catch (IOException e) {
            log.error(e.getMessage(), e);
        } finally {
            try {
                in.close();
                out.close();
            } catch (IOException e) {
                log.error(e.getMessage(), e);
            }
        }
    }


    public abstract void filter(Reader in, Writer out) throws IOException;
}
