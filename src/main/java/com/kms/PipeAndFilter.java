package com.kms;

import com.kms.filter.GrepFilter;
import com.kms.filter.Rot13Filter;
import com.kms.sink.SinkPipeWriter;
import com.kms.source.SourcePipeReader;
import lombok.extern.slf4j.Slf4j;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.io.Writer;

@Slf4j
public class PipeAndFilter {

    public static void main(String[] args) throws IOException {
        if (args.length != 2) {
            log.info("Usage: java PipeAndFilter <pattern> <filename>");
            System.exit(0);
        }

        Reader in = new BufferedReader(new FileReader(args[1]));
        Writer out = new BufferedWriter(new OutputStreamWriter(System.out));

        SinkPipeWriter sink = new SinkPipeWriter(out);
        PipeFilter filter3 = new Rot13Filter(sink);
        PipeFilter filter2 = new Rot13Filter(filter3);
        PipeFilter filter1 = new GrepFilter(filter2, args[0]);

        SourcePipeReader source = new SourcePipeReader(in, filter1);

        log.info("Pipeline is going to start filtering file {} using pattern [{}]", args[1], args[0]);
        source.startPipe();

        try {
            source.joinPipe();
        } catch (InterruptedException e) {
            log.error(e.getMessage(), e);
        }
    }
}
