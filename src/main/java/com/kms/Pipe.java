package com.kms;

import java.io.IOException;
import java.io.PipedReader;
import java.io.PipedWriter;

/**
 * @author hnguyen created on 10/22/18.
 */
public abstract class Pipe extends Thread {
    protected PipedReader in;
    protected PipedWriter out;
    private Pipe pipe;

    public Pipe() {
        super();
    }

    public Pipe(Pipe pipe) throws IOException {
        this.pipe = pipe;
        this.out = new PipedWriter();
        this.out.connect(pipe.getReader());
    }

    public void startPipe() {
        if (pipe != null) {
            pipe.startPipe();
        }

        this.start();
    }

    /**
     * Filters will be connected to each other so {@link #in} will be empty. We will need to initialize it.
     */
    public PipedReader getReader() {
        if (in == null) {
            in = new PipedReader();
        }

        return in;
    }

    public void joinPipe() throws InterruptedException {
        if (pipe != null) {
            pipe.joinPipe();
        }

        this.join();
    }
}
