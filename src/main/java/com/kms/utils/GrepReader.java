package com.kms.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.Reader;

/**
 * @author hnguyen created on 10/22/18.
 */
public class GrepReader extends BufferedReader {
    private String pattern;

    public GrepReader(Reader in, String pattern) {
        super(in);
        this.pattern = pattern;
    }

    @Override
    public final String readLine() throws IOException {
        String line;
        do {
            line = super.readLine();
        }
        while ((line != null) && !line.contains(pattern));
        return line;
    }
}
