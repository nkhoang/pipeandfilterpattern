package com.kms.utils;

/**
 * @author hnguyen created on 10/22/18.
 */
public final class Rot13 {
    public static char convert(char a) {
        char b = a;
        if ((a >= 'a') && (a <= 'z')) {
            b = (char) ('a' + ((a - 'a') + 13) % 26);
        }
        if ((a >= 'A') && (a <= 'Z')) {
            b = (char) ('A' + ((a - 'A') + 13) % 26);
        }

        return b;
    }
}
