package com.kms.sink;

import com.kms.Pipe;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.io.Writer;

/**
 * @author hnguyen created on 10/22/18.
 */

@Slf4j
public class SinkPipeWriter extends Pipe {
    private Writer writer;

    public SinkPipeWriter(Writer out) {
        this.writer = out;
    }

    @Override
    public void run() {
        if (log.isDebugEnabled()) {
            log.debug("{} is writing to Sink", this.getName());
        }
        try {
            char[] buffer = new char[1024];
            int charsRead;
            while ((charsRead = in.read(buffer)) != -1)
                writer.write(buffer, 0, charsRead);
        } catch (IOException e) {
            log.error(e.getMessage(), e);
        } finally {
            try {
                in.close();
                writer.flush();
            } catch (IOException e) {
                log.error(e.getMessage(), e);
            }
        }
    }

}
