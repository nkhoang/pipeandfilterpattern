package com.kms.source;

import com.kms.Pipe;
import com.kms.PipeFilter;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.io.Reader;

/**
 * @author hnguyen created on 10/22/18.
 */
@Slf4j
public class SourcePipeReader extends Pipe {
    private Reader reader;

    public SourcePipeReader(Reader reader, PipeFilter filter) throws IOException {
        super(filter);

        this.reader = reader;
    }


    @Override
    public void run() {
        if (log.isDebugEnabled()) {
            log.debug("{} is reading from Source", this.getName());
        }
        try {
            char[] buffer = new char[1024];
            int charsRead;
            while ((charsRead = reader.read(buffer)) != -1)
                out.write(buffer, 0, charsRead);
        } catch (IOException e) {
            log.error(e.getMessage(), e);
        }
        finally {
            try {
                reader.close();
                out.close();
            } catch (IOException e) {
                log.error(e.getMessage(), e);
            }
        }
    }
}
